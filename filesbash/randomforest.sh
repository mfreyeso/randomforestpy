#!/bin/bash
# -*- ENCODING: UTF-8 -*-

echo "Ingrese la direccion del directorio que se desea copiar al hdfs"
read fileforest
hdfs dfs -copyFromLocal $fileforest /
echo "Se copiaron con exito"
echo "Ingrese la direccion de destino para guardar el metadato (.info), se recomienda el directorio copiado previamente al hdfs."
read namemetadato
echo "Ingrese la direccion del archivo de entrenamiento copiado previamente."
read trainfile
echo "Ingrese de manera ordenada el tipo y numero de caracteristicas del arhivo de entrenamiento, ejemplo: archivo con 4 caracteristicas numericas y una nominal = 4 N L"
read featuresfile

cd /usr/local/mahout/core/mahout-distribution-0.9/
hadoop jar mahout-core-0.9-job.jar org.apache.mahout.classifier.df.tools.Describe -p $trainfile -f $namemetadato -d $featuresfile

echo "El metadato fue creado con exito."

echo "ENTRENAMIENTO...."
cd /usr/local/mahout/trunk/examples/target/

echo "Indique el directorio y nombre del bosque a crear"
read forest
inicio_ns=`date +%s%N`
inicio=`date +%s`
hadoop jar mahout-examples-1.0-SNAPSHOT-job.jar org.apache.mahout.classifier.df.mapreduce.BuildForest -Dmapred.max.split.size=1874231 -d $trainfile -ds $namemetadato -sl 5 -p -t 100 -o $forest
fin_ns=`date +%s%N`
fin=`date +%s`
let total_ns=$fin_ns-$inicio_ns
let total=$fin-$inicio
echo "El entrenamiento ha tardado: -$total_ns- nanosegudos, -$total- segundos"


echo "........Testing......."
echo "Ingrese la direccion del archivo de testing copiado previamente"
read testfile
echo "Ingrese el directorio y nombre para el informe final"
read finalfile
inicio_ns=`date +%s%N`
inicio=`date +%s`
hadoop jar mahout-examples-1.0-SNAPSHOT-job.jar org.apache.mahout.classifier.df.mapreduce.TestForest -i $testfile -ds $namemetadato -m $forest -a -mr -o $finalfile
fin_ns=`date +%s%N`
fin=`date +%s`
let total_ns=$fin_ns-$inicio_ns
let total=$fin-$inicio
echo "Las pruebas han tardado: -$total_ns- nanosegudos, -$total- segundos"