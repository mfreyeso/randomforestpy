import os
import random
import glob

import tablib
import numpy as np

dt = np.dtype("f2, f2, f2, f2, f2, i2, f2")
exdata = '../exdata'
trdata = '../trdata'
tsdata = '../tsdata'


list_data = list()
charplace = '%s/*.csv' % (exdata, )
namefiles = glob.glob(charplace)


def read_data():
    """
    Function that read files of a folder and append records in list_data list.
    """
    for namefile in namefiles:
        pathfile = os.path.abspath(namefile)
        data_array = np.genfromtxt(fname=pathfile, delimiter=',', dtype=dt)
        list_data.append(data_array)


def experiment_data(bind=0.7):
    '''
    This function select datafiles in directory data for extract and create
    new data files with samples for train and test model for each experiment.

    bind: (Float): This parameter allow the function percent of data
    for extract. Default is 0.7.
    '''

    for index, collection in enumerate(list_data):
        collection = list(collection)
        # Delete Headers
        del collection[0]
        # Extract sample data for train model with bind
        train_data = random.sample(
            collection, int(len(collection)*bind))
        # Class and write train sample data for the experiment in files
        label_data = class_data(train_data)
        write_data(label_data,
                   namefiles[index].split("/")[-1].split(".")[0].split("_")[1],
                   trdata)
        # Class and write train test data for the experimient in files
        test_data = random.sample(
            collection, int(len(collection)*(1-bind)))
        label_data = class_data(test_data)
        write_data(label_data,
                   namefiles[index].split("/")[-1].split(".")[0].split("_")[1],
                   tsdata, True)


def class_data(collection):
    """
    This function classifies the records in 'Y' when the magnitude record is
    different of 0.0, otherwise classifies N.
    Args:
        collection (iterator): A iterator that contains a set records chosen
        as sample.
    Returns:
        data (tablib): A tablib dataset object with records classified.
    """
    classdef = "Y"
    data = tablib.Dataset()
    for record in collection:
        if record[-1] == 0.00:
            classdef = "N"
        data.append((list(record) + list(classdef)))
        classdef = "Y"
    return data


def write_data(data, name, dirdata, option=False):
    """
    This function create a file with records classified for train and test
    model learning classification.

    Args:
        data (tablib): A tablib dataset object that contains classified
        records
        name (str): Name for file that store the records for train or test.
        dirdata (str): Path for store file.
        option (boolean): True if the file is a file thar contains test
        records.
    """
    path = dirdata + "/trdata_%s.csv" % (name, )
    if option == 1:
        path = dirdata + "/tsdata_%s.csv" % (name, )
    with open(path, 'wb') as f:
        f.write(bytes(data.csv, 'UTF-8'))


def main():
    read_data()
    experiment_data(0.65)

if __name__ == '__main__':
    main()
