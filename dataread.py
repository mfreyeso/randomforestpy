import csv
import numpy as np

'''
Este script es un ejemplo de las posibles formas de lectura para los archivos
iniciales de precipitación tomados de la bodega de datos. Ambos metodos
son funcionales; sin embargo el metodo que utiliza numpy es más lento que el
otro pero la ventaja ofrecida por numpy es la conversion del tipo de datos.
'''

dest_file = '../exdata/data_alcazares.csv'
dt = np.dtype("f4, f4, f4, f4, f4, i4, f4")

# Metodo Numpy

values = np.genfromtxt(fname=dest_file, delimiter=',', dtype=dt)


# Metodo Lectura CSV

with open(dest_file, 'r') as dest_f:
    data_iter = csv.reader(dest_f,
                           quotechar='"')
    data = [data for data in data_iter]
data_array = np.asarray(data)
for itype in data_array[1]:
    print(type(itype))
